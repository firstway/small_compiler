#!/bin/sh

codegen_stand=$1
codegen_to_be_test=$2

source_file=$3
iloc_name=iloc.out
EX1=ex.out_1
EX2=ex.out_2

tmp_dir=/tmp/diff_compiler_ex_result
mkdir -p ${tmp_dir}
cd ${tmp_dir}

rm ${iloc_name}
${codegen_stand} < ${source_file}
sim < ${iloc_name} | grep -v 'Executed' > ${EX1}


rm ${iloc_name}
${codegen_to_be_test} < ${source_file}
sim < ${iloc_name} | grep -v 'Executed' > ${EX2}

diff_count=$(diff ${EX1} ${EX2} | wc -l )
echo "ex $source_file, diff:${diff_count}" 
echo ''


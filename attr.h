/**********************************************
        CS515  Compiler  Project
        Fall  2013
#include "symtab.h"
#include "parse.tab.h"
**********************************************/

#ifndef ATTR_H
#define ATTR_H


typedef union {int num; char *str;} tokentype;

/* ^^^^^^^^^^^^^^^^^^^^^^^^^^node.h^^^^^^^^^^^^^^^^^^^^^^^^^^^^ */
typedef enum { typeCon, typeId, typeOpr } nodeEnum;


/* constants */
typedef struct {
	int value;
} conNodeType;


/* identifiers */
typedef struct {
	char* s;
} idNodeType;


/* operators */
typedef struct {
	int oper;
	int nops;
    int x;
    int y;/*  for array */
	struct nodeTypeTag **op;
} oprNodeType;


typedef struct nodeTypeTag {
	nodeEnum type;
	union {
		conNodeType con;
		idNodeType id;
		oprNodeType opr;
	};
} nodeType;


nodeType *opr_add_child(nodeType *parent, nodeType *child);
int compare_tree(nodeType *pA, nodeType *pB);

int INFO_LEVEL;


/* ^^^^^^^^^^^^^^^^^^^^optim_cache.h^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ */
/* store node and register */
typedef struct {
     nodeType *p;
     int reg;
} CacheEntry;

int lookup_cache(nodeType *p);
int insert_cache(nodeType *p, int reg);
int enable_cache();
int disable_cache();



int optimize_flag; 
int print_node(nodeType *p);
int gen(nodeType *p);
void enable_optim();

#endif


/**********************************************
        CS515  Compilers  Project1
        Fall  2013
**********************************************/

#ifndef SYMTAB_H
#define SYMTAB_H

#include <string.h>

/* The symbol table implementation uses a single hash     */
/* function. Starting from the hashed position, entries   */
/* are searched in increasing index order until a         */
/* matching entry is found, or an empty entry is reached. */

typedef struct {
  char *name;
  int vartype;
  int address;
  int x,y; /* for array */
  /* need to add stuff here */
} SymTabEntry;

void InitSymbolTable();

SymTabEntry * lookup(char *name);

SymTabEntry * insert(char *name);


void PrintSymbolTable();


#endif

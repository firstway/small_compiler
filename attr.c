/**********************************************
        CS515  Compiler  Project
        Fall  2013
**********************************************/


#include "attr.h" 

/* ----------------node.c----------------- */



#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>




#define SIZEOF_NODETYPE ((char *)&p->con - (char *)p)

extern void yyerror(char *s); 

nodeType *loc_node_type(){
	nodeType *p;
	if( NULL== (p= malloc(sizeof(nodeType))) ){
		yyerror("out of memory");
	}
	return p;
}

nodeType *con(int value){
	nodeType *p = loc_node_type();
	p->type = typeCon;
	p->con.value = value;
	return p;
}

nodeType *id(char* str){
	nodeType *p = loc_node_type();
	p->type = typeId;
	if( NULL== (p->id.s= malloc(strlen(str)+1)) ){
		yyerror("out of memory");
		return NULL;
	}
	strcpy(p->id.s, str);
	return p;	
}

nodeType *opr (int oper, int nops, ...){
	va_list ap;
	nodeType *p;
	int i;

	p = loc_node_type();
	if ((p->opr.op = malloc(nops * sizeof(nodeType))) == NULL){
		yyerror("out of memory");
        return NULL;
	}

	p->type = typeOpr;
	p->opr.oper = oper;
	p->opr.nops = nops;

	va_start(ap, nops);
	for (i = 0; i < nops; i++)
		p->opr.op[i] = va_arg(ap, nodeType*);
	va_end(ap);
	return p;
}

nodeType *opr_add_child(nodeType *parent, nodeType *child){
    int children_size;
    struct nodeTypeTag **new_op;

    children_size = parent->opr.nops + 1;
    new_op = malloc(children_size * sizeof(nodeType));
    if(!new_op){
		yyerror("out of memory");
        return NULL;
    }
    memcpy(new_op, parent->opr.op, parent->opr.nops* sizeof(nodeType) );
    new_op[children_size-1] = child;
    free(parent->opr.op);
    parent->opr.op = new_op;
    parent->opr.nops = children_size;
    return parent;
};

void freeNode(nodeType *p) {
	int i;
	if (!p) return;
	if (p->type == typeOpr) {
		for (i = 0; i < p->opr.nops; i++)
			freeNode(p->opr.op[i]);
		free(p->opr.op);
	}
	free (p);
}


int compare_tree(nodeType *pA, nodeType *pB){
    int i;

    if(pA == NULL && pB == NULL) return 0;
    if(pA->type != pB->type) return 1;

    if( typeCon == pA->type){
        return pA->con.value - pB->con.value;
    }else if(typeId == pA->type){
        return strcmp(pA->id.s, pB->id.s);
    }else if(typeOpr == pA->type){
        if(pA->opr.oper != pB->opr.oper) return 2;
        if(pA->opr.nops != pB->opr.nops) return 3;
		for (i = 0; i < pA->opr.nops; i++){
            if( 0 != compare_tree(pA->opr.op[i], pB->opr.op[i]) ) return 4;
        }
        return 0;
    }/* end of if */
    return -1;
};




/* ----------------graphtree.c----------------- */
#include "parse.tab.h"

char* node2str(nodeType *p, char* buf){
    strcpy (buf, "Null");
    if(!p) return buf;
    strcpy (buf, "???!!!");
    switch(p->type) {
        case typeCon: sprintf (buf, "constant(%d)", p->con.value); break;
        case typeId: sprintf (buf, "ID(%s)", p->id.s); break;
        case typeOpr:
                     switch(p->opr.oper){
                         /*case WHILE:
                             strcpy(buf, "while"); break; */
                         case WHILE:
                              strcpy(buf ,"while");
                             break;
                         case IF:
                              strcpy(buf ,"if");
                             break;
                         case WRITELN:
                              strcpy(buf ,"writeln");
                             break;
                         case IDLIST:
                              strcpy(buf ,"IDLIST");
                             break;
                         case INT:
                              strcpy(buf ,"int");
                             break;
                         case ARRAY:
                              strcpy(buf ,"[...]");
                             break;
                         case FOR:
                              strcpy(buf ,"for");
                             break;
                         case FOR_CTRL:
                              strcpy(buf ,"for_ctrl");
                             break;
                         case ';':
                              strcpy(buf , "[;]");
                             break;
                         case ASG:
                              strcpy(buf ,"[=]");
                             break;
                         case '+':
                              strcpy(buf , "[+]");
                             break;
                         case '-':
                              strcpy(buf, "[-]");
                             break;
                         case '*':
                              strcpy(buf , "[*]");
                             break;
                         case LEQ:
                              strcpy(buf, "[<=]");
                             break;
                         case LT:
                              strcpy(buf, "[<]");
                             break;
                         case GT:
                              strcpy(buf, "[>]");
                             break;
                         case GE:
                              strcpy(buf, "[>=]");
                             break;
                         case NEQ:
                             strcpy(buf, "[!=]");
                             break;
                         case EQ:
                             strcpy(buf,"[==]");
                             break;
                     }
                     break;
    }
    return buf;
};

int print_node(nodeType *p) {
    char word[32];
    char *s;
    int k;

    if(!p) return -1;
    printf("->%s(%p), children:(left to right)\t", node2str(p, word), p);
    for (k = 0; k < p->opr.nops; k++){
        printf("%s(%p)\t", node2str(p->opr.op[k], word), p->opr.op[k]);
    }
    printf("\n");
    
    for (k = 0; k < p->opr.nops; k++){
        if(typeOpr == p->opr.op[k]->type)print_node(p->opr.op[k]);
    }
    return 0;
}




/* ----------------optim_cache.c----------------- */


#include <stdio.h>


static int _CacheEntryUsed = 0;
static int _CacheEntrySize = 0;
static CacheEntry *_CacheEntryArray = NULL;
static _Enable = 0;

CacheEntry * alloct_copy(CacheEntry p[], int old_size, int new_size){
    int i;
    
    if(old_size >= new_size) return p;
    CacheEntry *p_new;
    p_new = (CacheEntry*)malloc(sizeof(CacheEntry)*new_size);
    for(i=0; i < old_size; i++){
        p_new[i].p = p[i].p;
        p_new[i].reg = p[i].reg;
    }

    return p_new;
};

int enable_cache(){ _Enable = 1;};
int disable_cache(){ _Enable= 0;};

int clear_cache(){
    _CacheEntryUsed = 0;
};

int lookup_cache(nodeType *p){
    int i,r ;

    if(!_Enable) return 0;
    if(_CacheEntryUsed <= 0) return -1;
    for(i=0; i < _CacheEntryUsed; i++){
        r = compare_tree(p, _CacheEntryArray[i].p);
        if(0 == r){
            printf("match cache [%p] -> [%p]->%d\n",p, _CacheEntryArray[i].p ,_CacheEntryArray[i].reg);
            return _CacheEntryArray[i].reg;
        }/* end if */
    }
    return -2;

};

int insert_cache(nodeType *p, int reg){

    CacheEntry *old_p;

    if(!_Enable) return 0;
    if(!_CacheEntryArray){
        _CacheEntrySize = 10;
        _CacheEntryArray = alloct_copy(NULL, 0, _CacheEntrySize);
        _CacheEntryUsed = 0;
    };
    
    if(_CacheEntryUsed >= _CacheEntrySize){
        old_p = _CacheEntryArray;
        _CacheEntryArray = alloct_copy(old_p, _CacheEntrySize, _CacheEntrySize*1.5);
        _CacheEntrySize = _CacheEntrySize*1.5;
        if(old_p) free(old_p);
    }
    _CacheEntryArray[_CacheEntryUsed].p = p;
    _CacheEntryArray[_CacheEntryUsed].reg = reg;
    _CacheEntryUsed ++;

    return 0;
}; 



/* ----------------gen_code_from_tree.c----------------- */
#include "instrutil.h"
#include "symtab.h"

/* for unit byte */
int CON_4_REG;

static int optim_code_enable = 0;

void enable_optim(){
    optim_code_enable = 1;
};

void init_gen()
{
    static int done = 0;
    if(!done){
        CON_4_REG = NextRegister();
        emit(NOLABEL, LOADI, 4, CON_4_REG, EMPTY);
        done = 1;
    }
};


int gen_array_var(nodeType *p, int right){
    SymTabEntry * var;
    int reg_dimen_1 , reg_dimen_2 , reg_tmp;

    /* p has three children, var, dimension-1, dimension-2(option) */
    var = lookup(p->opr.op[0]->id.s);
    /* one dimension, can be constant or exp */
    reg_dimen_1 = gen(p->opr.op[1]);

    /* calculate address offset */
    int reg_add_offset = -1;
    if(p->opr.nops > 2){
        /* two dimension
         * Column-major order
         * offset = x + y*NUMROWS
         * where NUMROWS represents the number of rows in the array
         * */
        emitComment("two dimension array");
        reg_dimen_2 = gen(p->opr.op[2]);
        int reg_array_offset = NextRegister();
        emit(NOLABEL, LOADI, var->x, reg_array_offset, EMPTY);
        reg_tmp = NextRegister();
        emit(NOLABEL, MULT, reg_dimen_2, reg_array_offset, reg_tmp); /*  y*NUMROWS */
        reg_array_offset = NextRegister();
        emit(NOLABEL, ADD, reg_dimen_1, reg_tmp, reg_array_offset);
        reg_tmp = NextRegister();
        emit(NOLABEL, MULT, CON_4_REG, reg_array_offset, reg_tmp);
        reg_add_offset = NextRegister();
        emit(NOLABEL, ADDI, reg_tmp, var->address, reg_add_offset);
    }else{/* one dimension */
        emitComment("one dimension array");
        int reg_mult = NextRegister();
        emit(NOLABEL, MULT, CON_4_REG, reg_dimen_1, reg_mult);
        reg_add_offset = NextRegister();
        emit(NOLABEL, ADDI, reg_mult, var->address, reg_add_offset);
    }

    if(right){/* right value, loadAO r0, r20    => r16 (r20 is offset) */
        reg_tmp = NextRegister();
        emit(NOLABEL, LOADAO, 0, reg_add_offset, reg_tmp);
        return reg_tmp;
    }else{ 
        /* left value 
         * add r0, r6   => r2  (r6 is offset)
         * */
        reg_tmp = NextRegister();
         emit(NOLABEL, ADD, 0, reg_add_offset, reg_tmp);
         return reg_tmp;
    }
    emitComment("----------end of array----------");
    return -5;
};

/* support varibale and array variable */
int gen_var(nodeType *p, int right){
    int reg;
    SymTabEntry * var;

    if(typeOpr == p->type){
        return gen_array_var(p, right);
    }

    if(typeId != p->type) return -2;
    if(right){
        /* right value
         * loadAI r0, 4     => r13
         * */
        emitComment("load variable right value");
        var = lookup(p->id.s);
        reg = NextRegister();
        emit(NOLABEL, LOADAI, 0, var->address, reg);
    }else{ 
        /* left value
         * return the reg which contains the address of variable 
         *
         *       addI r0, offset     => r1
         * */
        var = lookup(p->id.s);
        reg = NextRegister();
        emitComment("load variable left value");
        emit(NOLABEL, ADDI, 0, var->address, reg);
    }
    return reg;
};

int gen_for_ctrl(nodeType *p, int lable3_list[]){
    nodeType *var_node, *ibegin, *iend;
    SymTabEntry * var;
    int reg, reg_begin, reg_end, reg_plus;

    var_node = p->opr.op[0];
    ibegin = p->opr.op[1];
    iend = p->opr.op[2];
     var = lookup(var_node->id.s);

    reg_begin = gen(ibegin);
    reg = NextRegister();
    emit(NOLABEL, SUBI, reg_begin, 1, reg);/* set begin= begin-1*/

    emit(NOLABEL, STOREAI, reg, 0, var->address);/* inint i for ctrl*/
    reg_end = gen(iend);
    /* set header lable for FOR_ctrl */
    lable3_list[0] = NextLabel();
    emit(lable3_list[0], NOP, EMPTY, EMPTY, EMPTY);
    /* i++
     * loadAI r0, 8     => r23 
     * addI r23, 1    => r24 
     * storeAI r24    => r0, 8
     * */
    reg_plus = NextRegister();
    emit(NOLABEL, ADDI, gen_var(var_node,1)/*right value*/, 1, reg_plus);
    emit(NOLABEL,STOREAI, reg_plus, 0, var->address);
    
    /*   cmp_LE r9, r12     => r10
     *   cbr r10    => L1, L2
     * */
    lable3_list[1] = NextLabel();
    lable3_list[2] = NextLabel();
    reg = NextRegister();
    emit(NOLABEL,CMPLE, reg_plus, reg_end, reg); 
    emit(NOLABEL,CBR, reg, lable3_list[1], lable3_list[2]);/*conditional jump*/
    return 0; 
};

int gen_ifhead(nodeType *p, int lable3_list[]){
    int reg_bool;

    reg_bool = gen(p);
    /* lables for THEN ELSE*/
    lable3_list[0] = NextLabel();
    lable3_list[1] = NextLabel();
    lable3_list[2] = -3;/* in case */
    emit(NOLABEL,CBR, reg_bool, lable3_list[0], lable3_list[1]);/*conditional jump*/
    return 0;
};

int gen_arithmetic(struct nodeTypeTag **children, int Op){
    int reg, reg_1, reg_2;
    reg = gen(children[0]);
    reg_1 = gen(children[1]);
    reg_2 = NextRegister();
    emit(NOLABEL, Op, reg, reg_1, reg_2);    
    return reg_2;
};

int gen(nodeType *p){
    int k;
    int v;
    int reg, reg_1, reg_2;
    int temp_address;
    int lable3_list[3];
    
    init_gen();

    if(!p) return -1;
    if(typeCon == p->type){
        reg = lookup_cache(p); 
        if(reg >0) return reg;
        /* return the reg which contains the value of constant */
        v = p->con.value;
        reg = NextRegister();
        emitComment("load constant");
        emit(NOLABEL, LOADI, v, reg, EMPTY);
        insert_cache(p, reg);
        return reg;
    }else if(typeId == p->type){
        reg = lookup_cache(p); 
        if(reg >0) return reg;
        reg = gen_var(p, 1);/* right value */
        insert_cache(p, reg);
        return reg;
    }else if(typeOpr == p->type){
        if(ARRAY == p->opr.oper){
            reg = lookup_cache(p); 
            if(reg >0) return reg;
            reg = gen_var(p, 1);/* right value */
            insert_cache(p, reg);
            return reg;
        }else if(';' == p->opr.oper){
            for (k = 0; k < p->opr.nops; k++){
                gen(p->opr.op[k]);
            }
        }else if(ASG==p->opr.oper){
            reg = gen_var(p->opr.op[0], 0);/* left value */
            /* optimazation only for right side*/
            clear_cache();
            if(optim_code_enable) enable_cache();
            reg_1 = gen(p->opr.op[1]);
            disable_cache();
            clear_cache();
            /* store r6    => r5 */
            emit(NOLABEL, STORE, reg_1, reg, EMPTY);    
            return reg;/* return reg which contains the address of variable */
        }else if('*'==p->opr.oper){
            reg = lookup_cache(p); 
            if(reg >0) return reg;
            emitComment("excute *");
            reg = gen_arithmetic(p->opr.op, MULT);
            insert_cache(p, reg);
            return reg; /* return reg which contains the value of MULT */
        }else if('+'==p->opr.oper){
            reg = lookup_cache(p); 
            if(reg >0) return reg;
            emitComment("excute +");
            reg = gen_arithmetic(p->opr.op, ADD);
            insert_cache(p, reg);
            return reg; /* return reg which contains the value of sum */
        }else if('-'==p->opr.oper){
            reg = lookup_cache(p); 
            if(reg >0) return reg;
            emitComment("excute -");
            reg = gen_arithmetic(p->opr.op, SUB);
            insert_cache(p, reg);
            return reg; /* return reg which contains the value of sub */
        }else if(WRITELN==p->opr.oper){
            reg = gen(p->opr.op[0]);
            temp_address = NextOffset(1);
            emit(NOLABEL,STOREAI, reg, 0, temp_address);
            emit(NOLABEL,OUTPUT,STATIC_AREA_ADDRESS+temp_address,EMPTY,EMPTY);
        }else if(FOR==p->opr.oper){
            emitComment("code for FOR loop");
            gen_for_ctrl(p->opr.op[0], lable3_list);
            emit(lable3_list[1], NOP, EMPTY, EMPTY, EMPTY);
            gen(p->opr.op[1]);
            emit(NOLABEL,BR, lable3_list[0],EMPTY,EMPTY);/* branch to ctrl*/
            emit(lable3_list[2], NOP, EMPTY, EMPTY, EMPTY);
        }else if(EQ==p->opr.oper){
            reg = gen(p->opr.op[0]);
            reg_1 = gen(p->opr.op[1]);
            reg_2 = NextRegister();
            emitComment("excute ==");
            emit(NOLABEL, CMPEQ, reg, reg_1, reg_2);    
            return reg_2; /* return reg which contains the bool value*/
        }else if(NEQ==p->opr.oper){
            reg = gen(p->opr.op[0]);
            reg_1 = gen(p->opr.op[1]);
            reg_2 = NextRegister();
            emitComment("excute !=");
            emit(NOLABEL, CMPNE, reg, reg_1, reg_2);    
            return reg_2; /* return reg which contains the bool value*/
        }else if(LT==p->opr.oper){
            reg = gen(p->opr.op[0]);
            reg_1 = gen(p->opr.op[1]);
            reg_2 = NextRegister();
            emitComment("excute <");
            emit(NOLABEL, CMPLT, reg, reg_1, reg_2);    
            return reg_2; /* return reg which contains the bool value*/
        }else if(LEQ==p->opr.oper){
            reg = gen(p->opr.op[0]);
            reg_1 = gen(p->opr.op[1]);
            reg_2 = NextRegister();
            emitComment("excute <=");
            emit(NOLABEL, CMPLE, reg, reg_1, reg_2);    
            return reg_2; /* return reg which contains the bool value*/
        }else if(GT==p->opr.oper){
            reg = gen(p->opr.op[0]);
            reg_1 = gen(p->opr.op[1]);
            reg_2 = NextRegister();
            emitComment("excute >");
            emit(NOLABEL, CMPGT, reg, reg_1, reg_2);    
            return reg_2; /* return reg which contains the bool value*/
        }else if(GE==p->opr.oper){
            reg = gen(p->opr.op[0]);
            reg_1 = gen(p->opr.op[1]);
            reg_2 = NextRegister();
            emitComment("excute >=");
            emit(NOLABEL, CMPGE, reg, reg_1, reg_2);    
            return reg_2; /* return reg which contains the bool value*/
        }else if(IF==p->opr.oper){
            int has_else = p->opr.nops > 2 ? 1 : 0;
            emitComment("code for IF block");
            gen_ifhead(p->opr.op[0], lable3_list);
            /* THEN block */
            emit(lable3_list[0], NOP, EMPTY, EMPTY, EMPTY);
            gen(p->opr.op[1]);
            if(has_else){
                lable3_list[2] = NextLabel(); 
                /* jump outside */
                emit(NOLABEL, BR, lable3_list[2], EMPTY, EMPTY);
                /* ELSE block */
                emit(lable3_list[1], NOP, EMPTY, EMPTY, EMPTY);
                gen(p->opr.op[2]);
                /* outside lable  */
                emit(lable3_list[2], NOP, EMPTY, EMPTY, EMPTY);
            }else{
                emit(lable3_list[1], NOP, EMPTY, EMPTY, EMPTY);
            }
        }else if(WHILE==p->opr.oper){
            emitComment("code for WHILE");
            lable3_list[0] = NextLabel();
            lable3_list[1] = NextLabel();
            lable3_list[2] = NextLabel();
            emit(lable3_list[0], NOP, EMPTY, EMPTY, EMPTY);/* head of loop */
            int reg_bool = gen(p->opr.op[0]);/* condexp */
            emit(NOLABEL,CBR, reg_bool, lable3_list[1], lable3_list[2]);/*conditional jump*/
            emit(lable3_list[1], NOP, EMPTY, EMPTY, EMPTY);/* head of body */
            gen(p->opr.op[1]); /* body */
            emit(NOLABEL, BR, lable3_list[0], EMPTY, EMPTY); /* jump back */
            emit(lable3_list[2], NOP, EMPTY, EMPTY, EMPTY);/* tail of body */
        }else{
            fprintf(stderr, "unknow operate:%c\n",p->opr.oper);
        }
    }else{
        fprintf(stderr, "unknow node type\n");
    }


    return -1;
};



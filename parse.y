

%{
#include <stdio.h>
#include "symtab.h"
#include "instrutil.h"
#include "attr.h"
int yylex();
void yyerror(char * s);

FILE *outfile;
char *CommentBuffer;
extern int build_from_tree(nodeType *p);
 
%}

%union {
	int iValue;
	char* sStr;
	nodeType *nPtr;
}

%token PROG PERIOD VAR IDLIST 
%token INT WRITELN THEN IF WHILE 
%token DO ARRAY OF
%token BEG END ASG  
%token EQ NEQ LT LEQ GT GE 
%token ELSE
%token FOR FOR_CTRL
%token <iValue> ICONST
%token <sStr> ID
%type <nPtr> block cmpdstmt stmtlist stmt fstmt astmt writestmt exp ctrlexp lhs wstmt
%type <nPtr> variables vardcls vardcl idlist type
%type <nPtr> ifstmt ifhead condexp

%start program

%nonassoc EQ NEQ LT LEQ 
%left '+' '-' 
%left '*' 

%nonassoc THEN
%nonassoc ELSE

%%
program : {emitComment("Assign STATIC_AREA_ADDRESS to register \"r0\"");
           emit(NOLABEL, LOADI, STATIC_AREA_ADDRESS, 0, EMPTY);} 
           PROG ID ';' block PERIOD { }
	;

block	: variables cmpdstmt {$$=$2;
        if(INFO_LEVEL) print_node($2);
        gen($2); }
	;

variables: /* empty */
	| VAR vardcls { if(INFO_LEVEL) print_node($2);
    build_from_tree($2); }
	;

vardcls	: vardcls vardcl ';' { $$ =opr_add_child($1,$2); }
	| vardcl ';' { $$=opr(';',1,$1); }
	| error ';' { yyerror("***Error: illegal variable declaration\n");}  
	;

vardcl	: idlist ':' type { $$ = opr_add_child($3, $1); }
	;

type	: INT 	{$$=opr(INT,0); $$->opr.x =0; $$->opr.y = 0;}
	| ARRAY '[' ICONST ']' OF INT	{ $$=opr(INT,0); 
            $$->opr.x = $3; $$->opr.y = 0; }	
        | ARRAY '[' ICONST ',' ICONST ']' OF INT {$$=opr(INT,0); 
            $$->opr.x = $3; $$->opr.y = $5; }
	;

idlist	: idlist ',' ID { /*insert($3);*/ $$ = opr_add_child($1,id($3));/* set all
       IDs to be children of idlist*/}
        | ID		{ /*insert($1);*/ $$=opr(IDLIST,1, id($1) ); }
	;

cmpdstmt: BEG stmtlist END {$$=$2; }
	;

stmtlist : stmtlist ';' stmt {$$=opr(';',2,$1,$3); }
	    | stmt {$$=$1; }
        | error { yyerror("***Error: ';' expected or illegal statement \n");}
	;

stmt    : ifstmt {$$=$1; }
	| fstmt {$$=$1; }
	| astmt {$$=$1; }
	| writestmt {$$=$1; }
	| cmpdstmt {$$=$1; }
    | wstmt    {$$=$1; }
	;

wstmt : WHILE condexp DO stmt { $$ = opr(WHILE,2, $2, $4); }
      ;

ifstmt :  ifhead 
          THEN stmt {$$ = opr(IF,2, $1, $3);} 
        | ifhead THEN stmt ELSE stmt {$$ = opr(IF,3, $1, $3, $5);}
	;

ifhead : IF condexp {$$=$2; }
        ;

writestmt: WRITELN '(' exp ')' {$$=opr(WRITELN,1,$3); }
	;

fstmt	: FOR ctrlexp DO stmt {$$ = opr(FOR,2, $2, $4); } 
	;

astmt : lhs ASG exp {$$ = opr(ASG,2,$1,$3); }
	;

lhs	:      ID			    {$$ = id($1); }
        |  ID '[' exp ']'	        {$$=opr(ARRAY,2,id($1),$3); }
        |  ID '[' exp ',' exp ']'   {$$=opr(ARRAY,3,id($1),$3,$5); }
        ;

exp	:   exp '+' exp		    {$$ = opr('+',2,$1,$3);} 
        | exp '-' exp		{$$ = opr('-',2,$1,$3); }
	    | exp '*' exp		{$$ = opr('*',2,$1,$3); }
        | ID			    {$$ = id($1); } 
        | ID '[' exp ']'	     {$$=opr(ARRAY,2,id($1),$3); }
        | ID '[' exp ',' exp ']' {$$=opr(ARRAY,3,id($1),$3,$5); }
        | ICONST                 {$$=con($1); }
        | error { yyerror("***Error: illegal expression\n");}  
	;


ctrlexp	: ID ASG ICONST ',' ICONST { $$=opr(FOR_CTRL, 3, id($1), con($3),
        con($5)); }
        ;


condexp	: exp NEQ exp		{$$ = opr(NEQ,2,$1,$3); }
	| exp EQ exp		{$$ = opr(EQ,2,$1,$3); }
	| exp LT exp		{$$ = opr(LT,2,$1,$3); }
	| exp LEQ exp		{$$ = opr(LEQ,2,$1,$3); }
	| exp GT exp		{$$ = opr(GT,2,$1,$3); }
	| exp GE exp		{$$ = opr(GE,2,$1,$3); }
	| error { yyerror("***Error: illegal conditional expression\n");}  
        ;

%%

void yyerror(char* s) {
        fprintf(stderr,"%s\n",s);
        }

int optimize_flag = 0;


int
main(int argc, char* argv[]) {


    int i;

    INFO_LEVEL = 0;
    for(i=1; i<argc; i++){
        if(strcmp(argv[i],"-O") == 0){
            optimize_flag = 1;
        }else if(strcmp(argv[i],"-v")==0){
            INFO_LEVEL = 1;
        }
    }

  if(INFO_LEVEL) printf("\n\tCS515 Fall 2013 Compiler author:Biao Li\n");
  if (optimize_flag ) {
    if(INFO_LEVEL) printf("\tVersion 1.0 CSE OPTIMIZER \n\n");
    enable_optim();
  }else{
    if(INFO_LEVEL) printf("\tVersion 1.0 (non-optimizing)\n\n");
  }
  outfile = fopen("iloc.out", "w");
  if (outfile == NULL) { 
    fprintf(stderr, "ERROR: cannot open output file \"iloc.out\".\n");
    return -1;
  }

  CommentBuffer = (char *) malloc(500);  
  InitSymbolTable();

  if(INFO_LEVEL) printf("1\t");
  yyparse();
  if(INFO_LEVEL) printf("\n");

  if(INFO_LEVEL) PrintSymbolTable();

  fclose(outfile);
  
  return 1;
}





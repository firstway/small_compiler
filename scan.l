%{
#include <string.h>
#include "attr.h"
#include "parse.tab.h"
#define YY_SKIP_YYWRAP
#define MY_ECHO if(INFO_LEVEL){ECHO;};
%}
	extern YYSTYPE yylval;
        extern int yywrap() {return 1;};
	extern int isatty();
	int lines = 1;

D       [0-9]
id	[A-Za-z][A-Za-z0-9]*

%%
{D}+		{MY_ECHO;
		yylval.iValue = atoi(yytext);
		return(ICONST);}
:=		{MY_ECHO;return(ASG);}
"=="		{MY_ECHO;return(EQ);}
"!="		{MY_ECHO;return(NEQ);}
"<="		{MY_ECHO;return(LEQ);}
"<"		    {MY_ECHO;return(LT);}
">"		    {MY_ECHO;return(GT);}
">="		{MY_ECHO;return(GE);}
\.		{MY_ECHO;return(PERIOD);}
"/*"		{
		int c1 = 0; int c2 = input();
		MY_ECHO;
		for(;;) {
			if (c2 == EOF) {
				printf("ERROR: EOF detected in comment\n");
				yyterminate();
				}
			if(INFO_LEVEL) fprintf(yyout,"%c",c2);
			if (c1 == '*' && c2== '/') break;
			c1 = c2;
			c2 = input();
			if (c1 == '\n' && c2 != EOF)
				{lines++; if(INFO_LEVEL) fprintf(yyout,"%d\t",lines);}
			}
		}
[\[\]*+(),:;=-]	{MY_ECHO;return(yytext[0]);}
program		{MY_ECHO;return(PROG);}
var		{MY_ECHO;return(VAR);}
integer		{MY_ECHO;return(INT);}
for		{MY_ECHO;return(FOR);}
do		{MY_ECHO;return(DO);}
array		{MY_ECHO;return(ARRAY);}
of		{MY_ECHO;return(OF);}
begin		{MY_ECHO;return(BEG);}
end		{MY_ECHO;return(END);}
while		{MY_ECHO;return(WHILE);}
if		{MY_ECHO;return(IF);}
then		{MY_ECHO;return(THEN);}
else		{MY_ECHO;return(ELSE);}
writeln		{MY_ECHO;return(WRITELN);}
{id}		{MY_ECHO;
		yylval.sStr= (char *) malloc(strlen(yytext)+1);  strcpy(yylval.sStr, yytext);
		return(ID);
		}
\n		{int c;
		MY_ECHO;
		c = input();
		if (c != EOF) {unput(c); lines++; if(INFO_LEVEL) fprintf(yyout,"%d\t",lines);}
		else yyterminate();
		}
[ \t]		{MY_ECHO;}
[!@#$%^&|?]	{MY_ECHO;
		printf("\nERROR: %c is an illegal character\n",yytext[0]);
		}
.       {MY_ECHO;printf("\nERROR: %c is an illegal character\n",yytext[0]);}
%%

/**********************************************
        CS515  Compilers  Project1
        Fall  2013
**********************************************/

#include <stdio.h>
#include <strings.h>
#include "symtab.h"
#include "attr.h"
#include "parse.tab.h"

#define HASH_TABLE_SIZE 347

/* extern void * malloc(int size);  */

/*  --- STATIC VARIABLES AND FUNCTIONS --- */
static 
SymTabEntry **HashTable;

static
int hash(char *name) {
  int i;
  int hashValue = 1;
  
  for (i=0; i < strlen(name); i++) {
    hashValue = (hashValue * name[i]) % HASH_TABLE_SIZE;
  }

  return hashValue;
}


void
InitSymbolTable() {
  int i;
  int dummy;

  HashTable = (SymTabEntry **) malloc (sizeof(SymTabEntry *) * HASH_TABLE_SIZE);
  for (i=0; i < HASH_TABLE_SIZE; i++)
    HashTable[i] = NULL;
}


/* Returns pointer to symbol table entry, if entry is found */
/* Otherwise, NULL is returned */
SymTabEntry * 
lookup(char *name) {
  int currentIndex;
  int visitedSlots = 0;
  
  currentIndex = hash(name);
  while (HashTable[currentIndex] != NULL && visitedSlots < HASH_TABLE_SIZE) {
    if (!strcmp(HashTable[currentIndex]->name, name) )
      return HashTable[currentIndex];
    currentIndex = (currentIndex + 1) % HASH_TABLE_SIZE; 
    visitedSlots++;
  }
  return NULL;
}

/*if exist, return this one*/
SymTabEntry* insert(char *name) {
  int currentIndex;
  int visitedSlots = 0;
  SymTabEntry* sp;

  sp = lookup(name);
  if(sp) return sp;

  currentIndex = hash(name);
  while (HashTable[currentIndex] != NULL && visitedSlots < HASH_TABLE_SIZE) {
    if (!strcmp(HashTable[currentIndex]->name, name) ) 
      printf("*** WARNING *** in function \"insert\": %s has already an entry\n", name);
    currentIndex = (currentIndex + 1) % HASH_TABLE_SIZE; 
    visitedSlots++;
  }
  if (visitedSlots == HASH_TABLE_SIZE) {
    printf("*** ERROR *** in function \"insert\": No more space for entry %s\n", name);
    return NULL;
  }
  
  HashTable[currentIndex] = (SymTabEntry *) malloc (sizeof(SymTabEntry));
  HashTable[currentIndex]->name = (char *) malloc (strlen(name)+1);
  strcpy(HashTable[currentIndex]->name, name);
  return HashTable[currentIndex];
}

int var_unit_size(int arrayx, int arrayy){
    int unit_size = 1;
    if(arrayx > 0){
        unit_size = arrayx;
        if(arrayy>0){
            unit_size = arrayx * arrayy;
        }
    }
    return unit_size;
};

int add_id_list(int vartype, nodeType **idlist, int list_size, int arrayx, int arrayy){
    int k;
    nodeType *idp;
    char *var_name;
    SymTabEntry* sp;

    for (k = 0; k < list_size; k++){
        idp = idlist[k];
        var_name = idp->id.s;
        sp = insert(var_name);
        sp->vartype = vartype;
        sp->address = NextOffset(var_unit_size(arrayx,arrayy));
        sp->x = arrayx;
        sp->y = arrayy;
    }
    return 0;
};

int build_from_tree(nodeType *p){
    int k;
    nodeType *type_node;
    nodeType *idlist_node;

    if(!p) return -1;

    for (k = 0; k < p->opr.nops; k++){
        type_node = p->opr.op[k];
        if(typeOpr != type_node->type) continue;
        if(INT == type_node->opr.oper){
            idlist_node = type_node->opr.op[0];/* only one idlist node  */
            add_id_list(INT, idlist_node->opr.op, idlist_node->opr.nops, type_node->opr.x, type_node->opr.y); /*  */
        }
    }
    return 0;
};
void 
PrintSymbolTable() {
  int i;
  
  printf("\n --- Symbol Table ---------------\n\n");
  for (i=0; i < HASH_TABLE_SIZE; i++) {
    if (HashTable[i] != NULL) {
      printf("\"%s(x%d) array[%d,%d]\" \n", HashTable[i]->name, HashTable[i]->address, HashTable[i]->x, HashTable[i]->y); 
    }
  }
  printf("\n --------------------------------\n\n");
}


